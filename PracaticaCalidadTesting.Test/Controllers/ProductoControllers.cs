﻿using Moq;
using NUnit.Framework;
using practica.Interfaces.Interfaces;
using practicaCalidad.Controllers;
using practicaCalidad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using practicaCalidad.Models.Entities;

namespace PracaticaCalidadTesting.Test.Controllers
{
    [TestFixture]
    public class ProductoControllers
    {
        [Test]
        public  void TestIndexReturnView()
        {
            var mock= new Mock<InterfaceProducto>();
            mock.Setup(o => o.All()).Returns(new List<Producto>());
            var controller = new ProductoController(mock.Object);
            var view = controller.Index();

            Assert.IsInstanceOf(typeof(ViewResult),view);
            Assert.AreEqual("Inicio", view.ViewName);
            Assert.IsInstanceOf(typeof(List<Producto>), view.Model);

        }


    }
}
